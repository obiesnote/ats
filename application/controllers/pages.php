<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /**
	 * Written by Obinna Merenu
	 *
	 */
class Pages extends CI_Controller {

 
 
 function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('model_users');
		
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
	}
	
	
	/// loads the home page 
	public function home( )
	{
		$this->login();
	} 
	
	/// loads login page
	public function login()
	{  
		$user_access =  $this->model_users->get_email(); 
		
		if($this->session->userdata('is_logged_in') && $user_access == TRUE) 
		{ 
				$this->load->view('header');  
				$this->load->view('welcome');  
			}	
			else
			{
				$this->load->view('header');  
				$this->load->view('login');  
			}
	}
	
	// loads  validates login credentials
	public function validate_credentials()
	{
			 if ($this->model_users->can_log_in()){	
			 
					$user_name = $this->model_users->get_username($this->input->post('email'));
					$user_id = $this->model_users->get_userID($this->input->post('email'));
								
								$dataW = array(
									'username' => $user_name, 
									'is_logged_in' => true,
								    'user_id' => $user_id 
								);
								$this->session->set_userdata($dataW);			 
			 
				return true;
			 }
			 else {
			 $this->form_validation->set_message('validate_credentials','incorrect email/password');
			 return false;
			 }
	}	
	
	//  handles login validation
	 public function login_validation()
		{  
		 $this->form_validation->set_rules('email','Email','required|trim|xss_clean|callback_validate_credentials');
		 $this->form_validation->set_rules('password','Password','required|md5');

			if ($this->form_validation->run())
			{ 
				$data = array
				(
					'email'=> $this->input->post('email'),
					'is_logged_in'=>1
				);
				$this->session->set_userdata($data);
					$this->load->view('header');  
					$this->load->view('welcome');  
			}
			else{
				 $this->load->view('header');  
					$this->load->view('login');  
			}
		}	
		
	// Loads registration page
	public function register()
		{   
			$this->load->view('header'); 
			$this->load->view('register');  
		}
	 
	 //  handles registration validation 
	public function register_validation()
		{ 	  
			$this->form_validation->set_rules('username', 'Username','|trim|required|is_unique[users.username]|min_length[3]|max_length[12]|xss_clean|alpha_numeric');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric|password_check[1,1,1]|matches[passc]|md5');
			$this->form_validation->set_rules('passc', 'Password Confirmation', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[users.email]|is_unique[temp_users.email]|valid_email');
			$this->form_validation->set_message('is_unique','you are already signed up ');
			 
			if ($this->form_validation->run())
			{ 
			$key = md5(uniqid()); 
			$username  = $this->input->post('username');
			 
			if($this->model_users->add_temp_user($key))
				{ 
					echo "registered"; 
					$this->load->view('header');  
					$this->load->view('login'); 
			}
			else 
			{ 
				   echo " not registered";   
				$this->load->view('header');  
				$this->load->view('register'); 			   
			}
			}
			else{ 
				$this->load->view('header');  
				$this->load->view('register');  
			}
		}
	
	// logout
	 public function logout()
		{
		
			$this->session->sess_destroy();  
			$this->load->view('header');  
			$this->load->view('login'); 		
		}	

}