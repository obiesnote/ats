<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		/*
				Written by Obinna Merenu
				model handling db queries
		*/

class Model_users extends CI_Model{

	// checks if users has valid credentials to log in
	public function can_log_in()
	{
	
		$this->db->where('email', $this -> input->post('email'));
		$this->db->where('password',md5( $this -> input->post('password')));
		
		$query = $this->db->get('temp_users');
		if ($query->num_rows() == 1){
		return true;
		}
		else
			{		
			return false;
			}
	}
	
	 
	//add user temporarily to a database table during authentication
	public function add_temp_user($key)
	{
		$data = array(
		'username' => strtolower($this->input->post('username')),
		'email' => $this->input->post('email'),
		'password'=> $this->input->post('password') ,
		'key'=>$key
		);
	
		$query = $this->db->insert('temp_users',$data);
		if($query){
		return 
			true;
	      }
		  else  {
			return false;
			}
	}
	
 
 public function get_userID($email)
	{$user_id=NULL;
		$user_email = $this->session->userdata('email'); //get current users id from session
		$this->db->where('email',$user_email);
		$query = $this->db->get('users');       
		foreach ($query->result() as $row)
			{
				$user_id = $row->id;
			}
			return $user_id;
		}  
		
	 
	public function get_username($email)
	{
			$user_email = $this->session->userdata('email'); //get current users id from session
		$this->db->where('email',$user_email);
		$user_name =NULL;
		$query = $this->db->get('users');       
		foreach ($query->result() as $row)
			{
				$user_name = $row->username;
			}
			return $user_name;
	}  
	
	
	public function get_email()
	{
	$user_email = $this->session->userdata('email'); //get current users id from session
	$this->db->where('email',$user_email);
	$query = $this->db->get('temp_users');       
		foreach ($query->result() as $row)
			{
				$email= $row->email;
			}
			return $email;
	}  
  
  
	public function is_key_valid($key )
	{
		$this->db->where('key',$key);
		$query = $this->db->get('temp_users');
		
		if ($query->num_rows() >=1){
			return true;
		}
		else return false;
	}
	  
	 
}//end of class